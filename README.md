# Meisterwerk Fullstack

This project consists of:

- A JSON API written in Go 1.18 under `/backend`
- A SPA written in Typescript using React/Router/Redux/MaterialUI under `/frontend`

## Installation

This project uses [Docker](https://www.docker.com) and [Docker Compose](https://github.com/docker/compose) to run the necessary services which consists of:

- backend, golang:1.18-alpine
- frontend, nginx:1.21.6-alpine
- db, postgres:14, database volume mounted at `.meisterwerk/db`

It also consists of an nginx proxy in order to use custom subdomains like `frontend.meisterwerk.localhost`.

### Production

Start the required containers using docker-compose and ensure they are up to date with the source code.

```bash
docker-compose up db
sleep 10 # just give some time for postgres to start 
docker-compose up -d
```

Now you should have a container for the database, backend and frontend at {frontend,backend}.meisterwerk.localhost.

Migrate the database:

```bash
docker-compose exec migrate migrate -path='/migrations' -database='postgres://meisterwerk:meisterwerk@db:5432/db?sslmode=disable' up
```

Now open your browser at `http://frontend.meisterwerk.localhost` and enjoy the project!

### Development

For development we also want to start the containers, mainly because of the postgresql database

```bash
docker-compose up -d
```

Now you can start the api and web with hot reload:

```bash
# backend
cd backend
go run main.go

# frontend
cd frontend
cp .env.production .env # change api server if needed
yarn && yarn start
```
