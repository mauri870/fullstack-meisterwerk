# Meisterwerk Backend

```bash
make build
```

Tests

```bash
make test
```

## Minimum Requirements

- Create a golang service (needs Go 1.18; see pkg/events/app.go)
- Connect it to an SQL database of choice (Postgresql 14)
- Create the `events` table in the database (see migrations directory)
    - `title`: text
    - `start_time`: timestamp
    - `end_time`: timestamp
    - `address`: text
    - `status`: text / enum (pending, in progress, done)
    - Bonus: populate with dummy data
- events CRUD REST endpoints (Create, List, Update, Delete) (see pkg/events/controller.go)
- Add request validations on create and update (see internal/dto/dto.go)
    - `title` required
    - `start_time` required
    - `end_time` required
    - `start_time` < end_time
    - `status` required, valid values are: `pending, in progress, done`

## Bonus Requirements

- Create an addresses table in the database and make the relation between it and the events table. Use address_id in events table to reference it (see migrations/)
    - `full_address`: text
- Add date filtering to the List endpoint (see pkg/events/controller.go)
`from` and `to` query parameters (timestamps)
respond with events between the timespan 
- Unit tests (as much as you see fit)
- Basic authentication in the endpoints, use a middleware (see pkg/events/controller.go)
- Log errors (`zap`, `logrus`, etc) (implemented for error logging with logrus)