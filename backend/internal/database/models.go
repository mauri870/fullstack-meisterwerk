package database

import "time"

type Event struct {
	Id        int       `db:"id" json:"id"`
	Title     string    `db:"title" json:"title"`
	StartTime time.Time `db:"start_time" json:"start_time"`
	EndTime   time.Time `db:"end_time" json:"end_time"`
	Status    string    `db:"status" json:"status"`
	AddressID int       `db:"address_id" json:"address_id"`
}

type Address struct {
	Id          int    `db:"id"`
	FullAddress string `db:"full_address"`
}
