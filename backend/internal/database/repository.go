//go:generate mockgen -destination=../../mocks/mock_repository.go -package mocks . IEventRepository
package database

import (
	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"gitlab.com/mauri870/go-meisterwerk/internal/dto"
)

type IEventRepository interface {
	Create(data dto.EventCreateDto) error
	List(data dto.EventListDto) ([]Event, error)
	Update(id int, data dto.EventUpdateDto) error
	Delete(id int) error
}

func NewEventRepository(db *sqlx.DB) IEventRepository {
	sqp := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	return &EventRepository{db, sqp}
}

type EventRepository struct {
	db  *sqlx.DB
	sqp sq.StatementBuilderType
}

func (r *EventRepository) Create(data dto.EventCreateDto) error {
	_, err := r.sqp.Insert("events").
		Columns("title", "start_time", "end_time", "status", "address_id").
		Values(
			data.Title,
			data.StartTime,
			data.EndTime,
			data.Status,
			data.AddressID,
		).
		RunWith(r.db).
		Exec()
	return err
}

func (r *EventRepository) List(data dto.EventListDto) ([]Event, error) {
	stmt := r.sqp.Select("*").
		From("events")

	hasFromAndTo := !data.From.IsZero() && !data.To.IsZero()
	if hasFromAndTo {
		stmt = stmt.Where(sq.Expr("start_time BETWEEN $1 AND $2", data.From, data.To))
	}

	q, args, err := stmt.ToSql()
	if err != nil {
		return nil, err
	}

	var events []Event
	err = r.db.Select(&events, q, args...)
	if err == sql.ErrNoRows {
		return []Event{}, nil
	}

	return events, err
}

func (r *EventRepository) Update(id int, data dto.EventUpdateDto) error {
	_, err := r.sqp.Update("events").
		Set("title", data.Title).
		Set("start_time", data.StartTime).
		Set("end_time", data.EndTime).
		Set("status", data.Status).
		Set("address_id", data.AddressID).
		Where(sq.Eq{"id": id}).
		RunWith(r.db).
		Exec()
	return err
}

func (r *EventRepository) Delete(id int) error {
	_, err := r.sqp.Delete("events").
		Where(sq.Eq{"id": id}).
		RunWith(r.db).
		Exec()
	return err
}
