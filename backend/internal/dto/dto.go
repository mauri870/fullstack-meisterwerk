package dto

import (
	"time"
)

type EventCreateDto struct {
	Title     string    `json:"title" validate:"required"`
	StartTime time.Time `json:"start_time" validate:"required,ltfield=EndTime"`
	EndTime   time.Time `json:"end_time" validate:"required"`
	Status    string    `json:"status" validate:"required,oneof=pending in-progress done"`
	AddressID int       `json:"address_id" validate:"required"`
}

type EventUpdateDto struct {
	Title     string    `json:"title" validate:"required"`
	StartTime time.Time `json:"start_time" validate:"required,ltfield=EndTime"`
	EndTime   time.Time `json:"end_time" validate:"required"`
	Status    string    `json:"status" validate:"required,oneof=pending in-progress done"`
	AddressID int       `json:"address_id" validate:"required"`
}

type EventListDto struct {
	From time.Time `json:"from" validate:"omitempty"`
	To   time.Time `json:"to" validate:"required_with=From,ltfield=From"`
}
