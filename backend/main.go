package main

import (
	"os"

	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mauri870/go-meisterwerk/internal/database"
	"gitlab.com/mauri870/go-meisterwerk/pkg/events"
)

func getEnv(name, fallback string) string {
	env := os.Getenv(name)
	if env == "" {
		return fallback
	}

	return env
}

func main() {
	// env
	if err := godotenv.Load(".env"); err != nil {
		log.Infoln("Skipping .env...")
	}

	// logging
	lvl, err := log.ParseLevel(getEnv("LOG_LEVEL", "INFO"))
	if err != nil {
		lvl = log.InfoLevel
	}
	log.SetLevel(lvl)

	// db
	db, err := database.Connect()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// application
	controllers := events.NewEventController(database.NewEventRepository(db))
	app := events.NewApp(controllers)
	log.Fatalln(app.Run())
}
