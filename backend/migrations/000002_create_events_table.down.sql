DROP INDEX IF EXISTS idx_event_status;
DROP INDEX IF EXISTS idx_event_start_time_end_time;

DROP TABLE IF EXISTS events;

DROP TYPE IF EXISTS event_status;