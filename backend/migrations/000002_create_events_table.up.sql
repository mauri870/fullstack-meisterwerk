-- HACK: Postgres does not have a CREATE OR REPLACE type
DROP TYPE IF EXISTS event_status;
CREATE TYPE event_status AS ENUM ('pending', 'in-progress', 'done');

CREATE TABLE IF NOT EXISTS events (
    id BIGSERIAL PRIMARY KEY,
    title TEXT,
    start_time TIMESTAMP DEFAULT NULL,
    end_time TIMESTAMP DEFAULT NULL,
    status event_status,
    address_id INT NOT NULL,
    CONSTRAINT fk_address FOREIGN KEY(address_id) REFERENCES addresses(id)
);

CREATE INDEX IF NOT EXISTS idx_events_status ON events (status);
CREATE INDEX IF NOT EXISTS idx_events_start_time_end_time ON events (start_time, end_time);