package events

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func NewApp(controller IEventController) *App {
	e := gin.Default()

	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"*"}
	config.AllowHeaders = []string{"*"}
	e.Use(cors.New(config))

	e.Use(AuthMiddleware())

	e.GET("/events", controller.List())
	e.POST("/events", controller.Create())
	e.PUT("/events/:id", controller.Update())
	e.DELETE("/events/:id", controller.Delete())

	return &App{engine: e}
}

type App struct {
	engine *gin.Engine
}

func (r *App) Run() error {
	return r.engine.Run()
}
