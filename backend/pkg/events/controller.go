package events

import (
	"errors"
	"io"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mauri870/go-meisterwerk/internal/database"
	"gitlab.com/mauri870/go-meisterwerk/internal/dto"
)

var (
	ErrInvalidEventId = errors.New("invalid event identifier")
	ErrUnauthorized   = errors.New("you don't have access to this resource")
)

type EventController struct {
	repo      database.IEventRepository
	validator *validator.Validate
}

type IEventController interface {
	Create() func(c *gin.Context)
	List() func(c *gin.Context)
	Update() func(c *gin.Context)
	Delete() func(c *gin.Context)
}

func NewEventController(repo database.IEventRepository) IEventController {
	return &EventController{repo: repo, validator: validator.New()}
}

func wrapGinError(c *gin.Context, status int, err error) {
	log.WithFields(log.Fields{
		"status": status,
		"err":    err,
	}).Error("An error occurred processing the request")
	c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
}

func wrapGinData(c *gin.Context, status int, data any) {
	c.JSON(status, gin.H{"data": data})
}

func (h *EventController) Create() func(c *gin.Context) {
	return func(c *gin.Context) {
		var data dto.EventCreateDto
		if err := c.ShouldBindJSON(&data); err != nil {
			wrapGinError(c, http.StatusBadRequest, err)
			return
		}

		if err := h.validator.Struct(data); err != nil {
			wrapGinError(c, http.StatusBadRequest, err.(validator.ValidationErrors))
			return
		}

		err := h.repo.Create(data)
		if err != nil {
			wrapGinError(c, http.StatusBadRequest, err)
			return
		}

		wrapGinData(c, http.StatusNoContent, nil)
	}
}

func (h *EventController) List() func(c *gin.Context) {
	return func(c *gin.Context) {
		var data dto.EventListDto
		if err := c.ShouldBindJSON(&data); err != nil && err != io.EOF {
			wrapGinError(c, http.StatusBadRequest, err)
			return
		}

		records, err := h.repo.List(data)
		if err != nil {
			wrapGinError(c, http.StatusBadRequest, err)
			return
		}

		wrapGinData(c, http.StatusOK, records)
	}
}

func (h *EventController) Update() func(c *gin.Context) {
	return func(c *gin.Context) {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			wrapGinError(c, http.StatusBadRequest, ErrInvalidEventId)
			return
		}

		var data dto.EventUpdateDto
		if err := c.ShouldBindJSON(&data); err != nil {
			wrapGinError(c, http.StatusBadRequest, err)
			return
		}

		if err := h.validator.Struct(data); err != nil {
			wrapGinError(c, http.StatusBadRequest, err.(validator.ValidationErrors))
			return
		}

		err = h.repo.Update(id, data)
		if err != nil {
			wrapGinError(c, http.StatusBadRequest, err)
			return
		}

		wrapGinData(c, http.StatusNoContent, nil)
	}
}

func (h *EventController) Delete() func(c *gin.Context) {
	return func(c *gin.Context) {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			wrapGinError(c, http.StatusBadRequest, ErrInvalidEventId)
			return
		}

		err = h.repo.Delete(id)
		if err != nil {
			wrapGinError(c, http.StatusBadRequest, err)
			return
		}

		wrapGinData(c, http.StatusNoContent, nil)
	}
}

func AuthMiddleware() func(*gin.Context) {
	return func(c *gin.Context) {
		auth := c.Request.Header.Get("Authorization")
		log.Println("Authorization is ", auth)

		if auth == "" {
			wrapGinError(c, http.StatusUnauthorized, ErrUnauthorized)
			c.Abort()
			return
		}

		token := auth[len("Bearer"):]
		if token == "" {
			wrapGinError(c, http.StatusUnauthorized, ErrUnauthorized)
			c.Abort()
			return
		}

		log.Println("Authorization token is: ", token)
		// authenticate token/user with a database...
		//

		c.Next()
	}
}
