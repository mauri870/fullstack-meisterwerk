#!/bin/sh

curl -XDELETE localhost:8080/events/1 \
    -H'Authorization: Bearer xxx' \
    -H'Accept: application/json' \
    -H'Content-Type: application/json'