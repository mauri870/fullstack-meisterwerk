#!/bin/sh

curl -XGET localhost:8080/events \
    -H'Authorization: Bearer xxx' \
    -H'Accept: application/json' \
    -H'Content-Type: application/json' \
    -d '{"from": "2022-04-11T20:10:51.52Z", "to": "2022-04-12T20:09:51.37Z"}'