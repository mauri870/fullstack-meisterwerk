#!/bin/sh

curl -XPUT localhost:8080/events/1 \
    -H'Authorization: Bearer xxx' \
    -H'Accept: application/json' \
    -H'Content-Type: application/json' \
    -d '{"title": "test updated", "start_time": "2022-04-11T20:10:51.52Z", "end_time": "2022-04-12T20:09:51.37Z", "status": "pending"}'