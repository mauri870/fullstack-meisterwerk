import React from "react";
import { Provider } from "react-redux";
import styled from "styled-components";
import { ConnectedRouter } from "connected-react-router";
import { PersistGate } from "redux-persist/integration/react";

import store, { persistor } from "./store";
import Router from "./router";
import history from "./router/history";

import "fontsource-roboto";
import { CssBaseline } from "@material-ui/core";

const Container = styled.div`
    width: 100wh;
    height: 100vh;
    display: flex;
    flex-flow: column nowrap;
`;

function App() {
    return (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <PersistGate loading={null} persistor={persistor}>
                    <Container>
                        <CssBaseline />
                        <Router />
                    </Container>
                </PersistGate>
            </ConnectedRouter>
        </Provider>
    );
}

export default App;
