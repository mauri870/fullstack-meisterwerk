import React from "react";
import { IconButton, Tooltip } from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { useHistory } from "react-router";

export const BackButton = () => {
    const history = useHistory();

    let handleBack = async (e: any) => {
        e.preventDefault();

        history.goBack();
    };

    return (
        <Tooltip title="Go Back">
            <IconButton onClick={handleBack}>
                <ArrowBackIosIcon />
            </IconButton>
        </Tooltip>
    );
};

export default BackButton;
