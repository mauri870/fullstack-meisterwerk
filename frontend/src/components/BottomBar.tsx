import React from "react";
import AppBar from "@material-ui/core/AppBar";
import { BottomNavigation, BottomNavigationAction } from "@material-ui/core";
import { List, CalendarToday } from "@material-ui/icons";
import { getViewMode } from "../store/app/selectors";
import { useDispatch, useSelector } from "react-redux";
import { setViewMode } from "../store/app/actions";
import { ViewMode } from "../store/app/types";

function BottomBar() {
    const dispatch = useDispatch();
    const viewMode = useSelector(getViewMode);
    return (
        <AppBar
            position={"fixed"}
            color="primary"
            style={{ width: "100%", top: "auto", bottom: 0 }}
        >
            <BottomNavigation
                value={viewMode}
                onChange={(event, newValue) =>
                    dispatch(setViewMode(newValue))
                }
                showLabels
            >
                <BottomNavigationAction
                    label="Calendar"
                    value={ViewMode.Calendar}
                    icon={<CalendarToday />}
                />
                <BottomNavigationAction
                    label="List"
                    value={ViewMode.List}
                    icon={<List />}
                />
            </BottomNavigation>
        </AppBar>
    );
}

export default BottomBar;
