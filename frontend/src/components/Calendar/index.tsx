import FullCalendar, { EventSourceInput } from "@fullcalendar/react";
import React, { useEffect, useState } from "react";
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
import { ButtonGroup, Container, Grid, IconButton, Tooltip } from "@material-ui/core";
import { CalendarEvent } from "../../store/events/types";
import { useHistory } from "react-router-dom";
import AddIcon from "@material-ui/icons/Add";

export interface CalendarProps {
    events: Array<CalendarEvent>;
}

export default function Calendar({ events }: CalendarProps) {
    const history = useHistory();
    return (
        <Container component="main" maxWidth="md">
            <Grid item xs={12}>
                <h1>Calendar</h1>
                <ButtonGroup variant="text">
                    <Tooltip title="Create">
                        <IconButton
                            onClick={() => history.push(`/events/create`)}
                        >
                            <AddIcon htmlColor="#66bb6a" />
                        </IconButton>
                    </Tooltip>
                </ButtonGroup>
            </Grid>

            <FullCalendar
                plugins={[dayGridPlugin]}
                initialView="dayGridMonth"
                events={events as EventSourceInput}
                eventClick={(ev) => {
                    ev.jsEvent.preventDefault();
                    history.push(`events/view/${ev.event.id}`);
                }}
            />
        </Container>
    );
}