import FullCalendar, { EventSourceInput } from "@fullcalendar/react";
import React, { useEffect, useState } from "react";
import { Container, Link } from "@material-ui/core";
import { CalendarEvent } from "../../store/events/types";

export interface EventsListProps {
    events: Array<CalendarEvent>;
}

export default function EventsList({ events }: EventsListProps) {
    return (
        <Container component="main" maxWidth="md">
            <h1>Event List</h1>
            <ul>
                {events.map((event) => (
                    <li key={event.id}>
                        <Link href={`events/view/${event.id}`}>{event.id}. {event.title} </Link>
                    </li>
                ))}
            </ul>
        </Container>
    );
}