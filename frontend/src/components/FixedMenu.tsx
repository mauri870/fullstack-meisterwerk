import React from "react";
import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Typography from "@material-ui/core/Typography";
import { ClickAwayListener } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            position: "absolute",
            width: "auto",
            zIndex: 99999,
            top: 300,
            left: 1000,
        },
    })
);

export const FixedMenu = ({
    show,
    top,
    left,
    actions,
    onClose,
}: {
    show: boolean;
    top: number;
    left: number;
    actions: any;
    onClose: any;
}) => {
    const classes = useStyles();

    if (!show) {
        return null;
    }

    return (
        <Paper className={classes.root} style={{ top, left }}>
            <ClickAwayListener onClickAway={onClose}>
                <MenuList>
                    {actions.map((v: any, i: number) => (
                        <MenuItem
                            onClick={() => {
                                v.handler();
                                onClose();
                            }}
                            key={i}
                        >
                            <ListItemIcon>{v.icon}</ListItemIcon>
                            <Typography variant="inherit">{v.name}</Typography>
                        </MenuItem>
                    ))}
                </MenuList>
            </ClickAwayListener>
        </Paper>
    );
};

export default FixedMenu;
