import React from "react";
import { CircularProgress, Container, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
}));

export const Loading = () => {
    const classes = useStyles();
    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.root}>
                <CircularProgress />
            </div>
        </Container>
    );
};

export default Loading;
