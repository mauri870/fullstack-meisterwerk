import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { Link } from "react-router-dom";
import UserAvatar from "./UserAvatar";
import { useDispatch, useSelector } from "react-redux";
import { getAuthUser } from "../store/auth/selectors";
import { Menu, MenuItem } from "@material-ui/core";
import { MoreVert } from "@material-ui/icons";
import { signout } from "../store/auth/actions";

const useStyles = makeStyles((theme) => ({
    menuButton: {
        marginRight: theme.spacing(2),
    },
    profileMenuButton: {
        color: "#ffffff",
    },
    avatar: {
        marginLeft: theme.spacing(1),
    },
    loginButton: {
        marginLeft: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

function Navbar() {
    const classes = useStyles();
    const dispatch = useDispatch();

    const user = useSelector(getAuthUser);

    const [menuAnchorEl, setMenuAnchorEl] = useState<null | HTMLElement>(null);

    const handleClickProfile = (event: React.MouseEvent<HTMLButtonElement>) => {
        setMenuAnchorEl(event.currentTarget);
    };

    const handleMenuClose = () => setMenuAnchorEl(null);

    const handleClickMenuLogout = () => {
        dispatch(signout());
        setMenuAnchorEl(null);
    };

    return (
        <AppBar position="sticky">
            <Toolbar>
                <Typography
                    component={Link}
                    to="/dashboard"
                    className={classes.title}
                    style={{ textDecoration: "none" }}
                    color="inherit"
                    variant="h6"
                >
                    Events
                </Typography>

                {user && (
                    <>
                        <UserAvatar
                            email={user!!.email}
                            style={{ margin: "10px", borderRadius: "50%" }}
                        />
                        <Typography variant="subtitle2">
                            {user!!.email}
                        </Typography>
                    </>
                )}

                <IconButton
                    className={classes.profileMenuButton}
                    aria-label="more"
                    aria-controls="long-menu"
                    aria-haspopup="true"
                    onClick={handleClickProfile}
                >
                    <MoreVert />
                </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={menuAnchorEl}
                    keepMounted
                    open={Boolean(menuAnchorEl)}
                    onClose={handleMenuClose}
                >
                    <MenuItem onClick={handleClickMenuLogout}>Logout</MenuItem>
                </Menu>
            </Toolbar>
        </AppBar>
    );
}

export default Navbar;
