import React from "react";
import Gravatar from "react-gravatar";

export const UserAvatar = (props: any) => {
    return (
        <Gravatar
            email={props.email}
            style={{ borderRadius: "50%" }}
            size={40}
            rating="pg"
            default="monsterid"
            protocol="https://"
            {...props}
        />
    );
};

export default UserAvatar;
