import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import PrivateRoute from "./PrivateRoute";
import { SignIn, Home } from "../screens";
import { CreateEvent } from "../screens/Events/CreateEvent";
import { EditEvent } from "../screens/Events/EditEvent";
import { ShowEvent } from "../screens/Events/ShowEvent";

export const Router = () => {
    return (
        <Switch>
            <PrivateRoute path="/dashboard" component={Home} />
            <Route path="/signin" component={SignIn} />

            <PrivateRoute exact path="/events/create" component={CreateEvent} />
            <PrivateRoute path="/events/edit/:id" component={EditEvent} />
            <PrivateRoute path="/events/view/:id" component={ShowEvent} />

            <PrivateRoute path="/">
                <Redirect to="/dashboard" />
            </PrivateRoute>

            <Redirect to="/signin" />
        </Switch>
    );
};

export default Router;
