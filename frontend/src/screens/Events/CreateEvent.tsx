import React, { useState } from "react";
import Container from "@material-ui/core/Container";
import { useHistory, useLocation } from "react-router-dom";

import BackButton from "../../components/BackButton";
import PostForm from "./PostForm";
import { EventDTO, EventStatusType } from "../../store/events/types";
import { useDispatch } from "react-redux";
import { createApiEvent } from "../../store/events/actions";

export const CreateEvent = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const location: any = useLocation();
    const initialData: EventDTO = { title: "", start_time: Date.now().toString(), end_time: Date.now.toString(), status: EventStatusType.Pending };

    const handleSubmit = async (event: EventDTO) => {
        dispatch(createApiEvent(event));
        history.replace("/dashboard");
    };

    return (
        <Container component="main" maxWidth="xs">
            <BackButton />
            <PostForm action={handleSubmit} initialData={initialData} />
        </Container>
    );
};

export default CreateEvent;
