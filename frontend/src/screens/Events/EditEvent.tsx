import React, { useEffect, useState } from "react";
import Container from "@material-ui/core/Container";
import { useHistory, useParams } from "react-router-dom";

import BackButton from "../../components/BackButton";
import PostForm from "./PostForm";
import { EventDTO } from "../../store/events/types";
import { getEventById } from "../../store/events/selectors";
import { useDispatch, useSelector } from "react-redux";
import { editApiEvent } from "../../store/events/actions";

export const EditEvent = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    let { id }: any = useParams();
    const initialData = useSelector(getEventById(id));


    const handleSubmit = async (ev: EventDTO) => {
        dispatch(editApiEvent(ev));
        history.replace("/dashboard");
    };

    return (
        <Container component="main" maxWidth="xs">
            <BackButton />
            <PostForm action={handleSubmit} initialData={initialData as EventDTO} />
        </Container>
    );
};

export default EditEvent;
