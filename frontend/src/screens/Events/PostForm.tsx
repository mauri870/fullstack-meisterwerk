import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

import { EventDTO, EventStatusType } from "../../store/events/types";
import { DateTimePicker } from "@material-ui/pickers";
import { InputLabel, MenuItem, Select } from "@material-ui/core";
import { enumKeys } from "../../helpers/enum";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export const PostForm = ({
    action,
    initialData,
}: {
    action: (post: EventDTO) => void;
    initialData: EventDTO;
}) => {
    const classes = useStyles();

    const [state, setState] = useState<EventDTO>(initialData);

    return (
        <div className={classes.paper}>
            <form
                className={classes.form}
                noValidate
                onSubmit={(e: any) => {
                    e.preventDefault();
                    action(state);
                }}
            >
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            name="title"
                            variant="outlined"
                            required
                            fullWidth
                            id="title"
                            label="Title"
                            autoFocus
                            value={state.title}
                            onChange={(e: any) =>
                                setState({
                                    ...state,
                                    title: e.target.value,
                                })
                            }
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <DateTimePicker
                            label="Start Date"
                            inputVariant="outlined"
                            showTodayButton
                            value={state.start_time}
                            onChange={(v: any) => {
                                setState({
                                    ...state,
                                    start_time: v,
                                })
                            }}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <DateTimePicker
                            label="End Date"
                            inputVariant="outlined"
                            showTodayButton
                            value={state.end_time}
                            onChange={(v: any) => {
                                setState({
                                    ...state,
                                    end_time: v,
                                })
                            }}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <InputLabel id="">Status</InputLabel>
                        <Select
                            value={state.status}
                            label="Status"
                            defaultValue={EventStatusType.Pending as string}
                            onChange={(ev: any) => {
                                setState({ ...state, status: ev.target.value })
                            }}
                        >
                            {enumKeys(EventStatusType).map(key => (
                                <MenuItem value={EventStatusType[key]}>{key}</MenuItem>
                            ))}
                        </Select>
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        SAVE
                    </Button>
                </Grid>
            </form>
        </div>
    );
};

export default PostForm;
