import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { useHistory, useParams } from "react-router-dom";

import BackButton from "../../components/BackButton";
import { getEventById } from "../../store/events/selectors";
import { useDispatch, useSelector } from "react-redux";
import { Event } from "../../store/events/types";
import { ButtonGroup, IconButton, Tooltip } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { deleteApiEvent } from "../../store/events/actions";
import ConfirmDialog from "../../components/ConfirmDialog";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export const ShowEvent = () => {
    const classes = useStyles();

    const dispatch = useDispatch();
    const history = useHistory();
    let { id }: any = useParams();
    const event = useSelector(getEventById(id));
    const [confirmDeleteOpen, setConfirmDeleteOpen] = useState<boolean>();

    const handleEditEvent = async (ev: Event) => {
        history.push(`/events/edit/${ev.id}`);
    };

    const handleDeleteEvent = async (ev: Event) => {
        dispatch(deleteApiEvent(ev.id));
        history.push('/dashboard');
    };

    if (event === undefined) {
        history.push('/dashboard');
    }

    return (
        <Container component="main" maxWidth="md">
            <BackButton />
            <div className={classes.paper}>
                <h1>
                    {event?.title}
                </h1>

                <h3>
                    Start time: {event?.start_time}
                </h3>

                <h3>
                    End time: {event?.end_time}
                </h3>

                <ConfirmDialog
                    title="Delete Event?"
                    open={confirmDeleteOpen}
                    setOpen={setConfirmDeleteOpen}
                    onConfirm={() => handleDeleteEvent(event!!)}
                >
                    Are you sure you want to delete this event?
                </ConfirmDialog>

                <ButtonGroup>
                    <Tooltip title="Delete">
                        <IconButton
                            onClick={() => setConfirmDeleteOpen(true)}
                        >
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>

                    <Tooltip title="Edit">
                        <IconButton
                            onClick={() =>
                                handleEditEvent(event!!)
                            }
                        >
                            <EditIcon />
                        </IconButton>
                    </Tooltip>
                </ButtonGroup>
            </div>
        </Container >
    );
};

export default ShowEvent;