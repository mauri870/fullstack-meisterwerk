import React, { useEffect, useState } from "react";

import styled from "styled-components";

import { useDispatch, useSelector } from "react-redux";

import Navbar from "../components/Navbar";
import { getViewMode } from "../store/app/selectors";
import BottomBar from "../components/BottomBar";
import Calendar from "../components/Calendar";
import { fetchEvents } from "../store/events/actions";
import EventsList from "../components/EventsList";
import { getEventsError, getEventsForCalendar, getIsEventsLoading } from "../store/events/selectors";
import Loading from "../components/Loading";
import { Alert, AlertTitle } from "@material-ui/lab";
import { ViewMode } from "../store/app/types";

const Container = styled.div`
    width: inherit;
    height: inherit;
`;

export const Home = () => {
    const dispatch = useDispatch();
    const events = useSelector(getEventsForCalendar);
    const loading = useSelector(getIsEventsLoading);
    const eventsError = useSelector(getEventsError);

    const viewMode = useSelector(getViewMode);

    useEffect(() => {
        (async () => {
            dispatch(fetchEvents());
        })();
    }, []);

    if (loading) {
        return <Loading />;
    }


    return (
        <>
            <Navbar />
            <Container>
                {eventsError && (
                    <Alert severity="error">
                        <AlertTitle>Error</AlertTitle>
                        {eventsError}
                    </Alert>
                )}
                {viewMode === ViewMode.Calendar ? <Calendar events={events} /> : <EventsList events={events} />}
            </Container>
            <BottomBar />
        </>
    );
};

export default Home;
