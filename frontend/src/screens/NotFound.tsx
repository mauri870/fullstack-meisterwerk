import React from "react";
import styled from "styled-components";
import Typography from "@material-ui/core/Typography";

const Container = styled.div`
    width: inherit;
    height: inherit;
`;

export const NotFound = () => {
    return (
        <Container>
            <Typography variant="h1" color="textSecondary" align="center">
                404
            </Typography>
            <Typography variant="h1" color="textSecondary" align="center">
                Nothing Here
            </Typography>
        </Container>
    );
};

export default NotFound;
