import { ViewMode, SET_VIEW_MODE, AppActionTypes } from './types';

export function setViewMode(mode: ViewMode): AppActionTypes {
    return {
        type: SET_VIEW_MODE,
        payload: mode,
    };
}

