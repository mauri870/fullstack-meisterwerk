import { AppState, AppActionTypes, ViewMode, SET_VIEW_MODE } from './types';

const initialState: AppState = {
    viewMode: ViewMode.Calendar,
};

export function appReducer(
    state = initialState,
    action: AppActionTypes
): AppState {
    switch (action.type) {
        case SET_VIEW_MODE:
            return {
                ...state,
                viewMode: action.payload,
            };
        default:
            return state;
    }
}
