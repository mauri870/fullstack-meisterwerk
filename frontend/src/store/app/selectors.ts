import { RootState } from '../';
import { ViewMode } from './types';

export function getViewMode(state: RootState): ViewMode {
    return state.app.viewMode;
}
