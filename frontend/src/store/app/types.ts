export const SET_VIEW_MODE = 'SET_VIEW_MODE';

export enum ViewMode {
    Calendar = "calendar",
    List = "list",
}

export interface AppState {
    viewMode: ViewMode;
}

interface SetViewMode {
    type: typeof SET_VIEW_MODE;
    payload: ViewMode,
}

export type AppActionTypes = SetViewMode;
