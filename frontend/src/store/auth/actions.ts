import { Token, SET_TOKEN, SET_USER, AuthActionTypes, User } from './types';
import { RESET_STORE } from '..';
import history from '../../router/history';

export function signin(data: object) {
    return async (dispatch: any) => {
        dispatch({ type: RESET_STORE });
        // HACK: do the actual api call to authenticate user
        dispatch(setUser({email: 'test@example.com', password: 'abc123'} as User))
        dispatch(setToken('abc123' as Token))
    };
}


export function signout() {
    return async (dispatch: any) => {
        dispatch({ type: RESET_STORE });
        history.replace('/signin');
    };
}

export function setToken(token: Token): AuthActionTypes {
    return {
        type: SET_TOKEN,
        payload: token,
    };
}

export function setUser(user: User): AuthActionTypes {
    return {
        type: SET_USER,
        payload: user,
    };
}
