import { AuthState, AuthActionTypes, SET_TOKEN, SET_USER } from './types';

const initialState: AuthState = {
    token: '',
    user: undefined,
};

export function authReducer(
    state = initialState,
    action: AuthActionTypes
): AuthState {
    switch (action.type) {
        case SET_TOKEN:
            return {
                ...state,
                token: action.payload,
            };
        case SET_USER:
            return {
                ...state,
                user: action.payload,
            };
        default:
            return state;
    }
}
