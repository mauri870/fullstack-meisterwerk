import { RootState } from '../';
import { User } from './types';

export function getAuthUser(state: RootState): User | undefined {
    return state.auth.user;
}
