export const SET_TOKEN = 'SET_TOKEN';
export const SET_USER = 'SET_USER';

export interface User {
    email: string;
    password: string;
};

export type Token = string;

export interface AuthState {
    user?: User;
    token: Token;
}

interface SetTokenAction {
    type: typeof SET_TOKEN;
    payload: Token;
}

interface SetUserAction {
    type: typeof SET_USER;
    payload: User;
}

export type AuthActionTypes = SetTokenAction | SetUserAction;
