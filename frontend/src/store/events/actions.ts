import { EventsActionTypes, SET_EVENTS_LOADING, Event, SET_EVENTS_LOADED, SET_EVENTS_ERROR, DELETE_EVENT, EDIT_EVENT, CREATE_EVENT, EventDTO } from './types';

export function fetchEvents() {
    return async (dispatch: any) => {
        try {
            dispatch(setEventsLoading());
            const result = await fetch(process.env.REACT_APP_API_URL + '/events', { 
                method: 'GET', 
                headers: new Headers({
                    'Authorization': 'Basic '+btoa('abc:123'),
                }),
            });

            const json = await result.json();
            return dispatch(setEventsLoaded(json.data || []));
        } catch (e: any) {
            dispatch(setEventsError(e.toString()))
        }
    };
}

export function createApiEvent(event: EventDTO) {
    return async (dispatch: any) => {
        try {
            await fetch(process.env.REACT_APP_API_URL + '/events', { 
                method: 'POST', 
                headers: new Headers({
                    'Authorization': 'Basic '+btoa('abc:123'),
                }),
                // HACK: Hardcoded address id
                body: JSON.stringify({...event, address_id: 1}),
            });

            return dispatch(fetchEvents());
        } catch (e: any) {
            dispatch(setEventsError(e.toString()))
        }
    };
}

export function editApiEvent(event: EventDTO) {
    return async (dispatch: any) => {
        try {
            await fetch(process.env.REACT_APP_API_URL + '/events/' + event.id, { 
                method: 'PUT', 
                headers: new Headers({
                    'Authorization': 'Basic '+btoa('abc:123'),
                }),
                body: JSON.stringify(event),
            });

            return dispatch(editEvent(event));
        } catch (e: any) {
            dispatch(setEventsError(e.toString()))
        }
    };
}

export function deleteApiEvent(id: number) {
    return async (dispatch: any) => {
        try {
            await fetch(process.env.REACT_APP_API_URL + '/events/' + id, { 
                method: 'DELETE', 
                headers: new Headers({
                    'Authorization': 'Basic '+btoa('abc:123'),
                }),
            });

            return dispatch(deleteEvent(id));
        } catch (e: any) {
            dispatch(setEventsError(e.toString()))
        }
    };
}

export function deleteEvent(id: number): EventsActionTypes {
    return {
        type: DELETE_EVENT,
        payload: id,
    };
}

export function editEvent(event: EventDTO): EventsActionTypes {
    return {
        type: EDIT_EVENT,
        payload: event
    };
}

export function createEvent(event: EventDTO): EventsActionTypes {
    return {
        type: CREATE_EVENT,
        payload: event,
    };
}

export function setEventsLoading(): EventsActionTypes {
    return {
        type: SET_EVENTS_LOADING,
    };
}

export function setEventsLoaded(events: Array<Event>): EventsActionTypes {
    return {
        type: SET_EVENTS_LOADED,
        payload: events,
    };
}

export function setEventsError(error: string | null): EventsActionTypes {
    return {
        type: SET_EVENTS_ERROR,
        payload: error,
    };
}
