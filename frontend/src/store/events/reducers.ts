import { EventsState, EventsActionTypes, SET_EVENTS_ERROR, SET_EVENTS_LOADED, SET_EVENTS_LOADING, DELETE_EVENT, EDIT_EVENT, CREATE_EVENT, Event } from './types';

const initialState: EventsState = {
    events: [],
    isLoading: false,
    error: null,
};

export function eventsReducer(
    state = initialState,
    action: EventsActionTypes
): EventsState {
    switch (action.type) {
        case SET_EVENTS_LOADING:
            return {
                ...state,
                isLoading: true,
                error: null,
            };
        case SET_EVENTS_LOADED:
            return {
                ...state,
                events: action.payload,
                isLoading: false,
                error: null,
            };
        case SET_EVENTS_ERROR:
            return {
                ...state,
                error: action.payload,
                isLoading: false,
            };
        case DELETE_EVENT:
            // HACK: This is only for demo purposes
            return {
                ...state,
                events: state.events.filter((ev) => ev.id != action.payload),
            }
        case EDIT_EVENT:
            // HACK: This is only for demo purposes
            return {
                ...state,
                events: state.events.map((ev) => {
                    if (ev.id == action.payload.id) {
                        return action.payload as Event;
                    }
                    return ev;
                }),
            }
        case CREATE_EVENT:
            // HACK: This is only for demo purposes
            return {
                ...state,
                events: [...state.events, { ...action.payload, id: state.events.length+2} as Event],
            }
        default:
            return state;
    }
}
