import { RootState } from '../';
import { CalendarEvent, Event, EVENT_COLORS } from './types';

export function getEvents(state: RootState): Array<Event> {
    return state.events.events || [];
}

export const getEventById = (id: number) => (state: RootState) : Event | undefined => {
    return state.events.events.find(ev => ev.id == id);
}

export function getEventsForCalendar(state: RootState): Array<CalendarEvent> {
    if (!state.events.events) {
        return [];
    }
    return state.events.events.map((event: Event) => {
        return {
            id: event.id,
            title: event.title,
            start: event.start_time,
            end: event.end_time,
            color: EVENT_COLORS[event.status],
        };
    });
}

export function getIsEventsLoading(state: RootState): boolean {
    return state.events.isLoading;
}

export function getEventsError(state: RootState): string | null {
    return state.events.error;
}
