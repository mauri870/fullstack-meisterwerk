export const SET_EVENTS_LOADED = 'SET_EVENTS_LOADED';
export const SET_EVENTS_LOADING = 'SET_EVENTS_LOADING';
export const DELETE_EVENT = 'DELETE_EVENT';
export const CREATE_EVENT = 'CREATE_EVENT';
export const EDIT_EVENT = 'EDIT_EVENT';
export const SET_EVENTS_ERROR = 'SET_EVENTS_ERROR';

export interface EventDTO {
    id?: number;
    title: string;
    start_time: string;
    end_time: string;
    status: EventStatusType;
} 

export enum EventStatusType {
    Pending = "pending",
    InProgress = "in-progress",
    Done = "done",
};

export const EVENT_COLORS: Record<EventStatusType, string> = {
    [EventStatusType.Pending]: "red",
    [EventStatusType.InProgress]: "grey",
    [EventStatusType.Done]: "green",
};

// TODO: CalendarEvent may extend Event
export interface CalendarEvent {
    id: number;
    title: string;
    start: string;
    end: string;
    color: string;
}

export interface Event {
    id: number;
    title: string;
    start_time: string;
    end_time: string;
    status: EventStatusType;
    address_id: number;
}

export interface EventsState {
    events: Array<Event>;
    isLoading: boolean;
    error: string | null;
}

interface SetEventsLoaded {
    type: typeof SET_EVENTS_LOADED;
    payload: Array<Event>;
}

interface SetEventsLoading {
    type: typeof SET_EVENTS_LOADING;
}

interface SetEventsError {
    type: typeof SET_EVENTS_ERROR;
    payload: string | null,
}

interface DeleteEvent {
    type: typeof DELETE_EVENT;
    payload: number, 
}

interface CreateEvent {
    type: typeof CREATE_EVENT;
    payload: EventDTO, 
}

interface EditEvent {
    type: typeof EDIT_EVENT;
    payload: EventDTO, 
}

export type EventsActionTypes = SetEventsLoaded | SetEventsLoading | SetEventsError | DeleteEvent | EditEvent | CreateEvent;
