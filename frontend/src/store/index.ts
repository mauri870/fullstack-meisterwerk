import { combineReducers, createStore, applyMiddleware, Action, compose } from 'redux';
import storage from 'redux-persist/lib/storage'
import { persistStore, persistReducer } from 'redux-persist';
import { connectRouter, routerMiddleware } from 'connected-react-router'
import thunk, { ThunkAction } from 'redux-thunk';

import history from '../router/history';
import { authReducer } from './auth/reducers';
import { appReducer } from './app/reducers';
import { eventsReducer } from './events/reducers';

// HACK: https://stackoverflow.com/questions/54371096/redux-thunk-property-type-missing-when-calling-action-through-store-dispatch
declare module 'redux' {
  interface Dispatch<A extends Action = AnyAction> {
    <S, E, R>(asyncAction: ThunkAction<R, S, E, A>): R;
  }
}

export const RESET_STORE = 'RESET_STORE';
export const resetStore = () => ({ type: RESET_STORE });

const rootAppReducer = (routerHistory: any) => combineReducers({
    router: connectRouter(routerHistory),
    auth: authReducer,
    events: eventsReducer,
    app: appReducer,
});

export const rootReducer = (routerHistory: any) => {
    return (state: any, action: Action) => {
        if (action.type === RESET_STORE) {
            state = undefined;
        }

        return rootAppReducer(routerHistory)(state, action);
    }
};

const persistedReducer = persistReducer({ key: 'root', storage, blacklist: ['router'] }, rootReducer(history));

let store = createStore(persistedReducer, compose(
    applyMiddleware(routerMiddleware(history)),
    applyMiddleware(thunk),
));
export default store;

export let persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>;
